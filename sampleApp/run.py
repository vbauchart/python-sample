"""program entrypoint"""

from sampleApp.sample import Sample


def main():
    """program entrypoint"""

    print("Starting Sample App")
    language = "python"
    message = Sample(language)
    print(f"super {message}")


if __name__ == "__main__":
    main()
