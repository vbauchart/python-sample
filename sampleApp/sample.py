"""Sample class"""


class Sample:
    """Class that manage a message"""

    def __init__(self, message="world"):
        self.message = message

    def __repr__(self):
        return self.message
